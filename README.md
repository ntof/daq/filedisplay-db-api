# FileDisplay db api

FileDisplay db api server

This application is developped according to [EN-SMM-APC guidelines](https://apc-dev.web.cern.ch/docs/drafts/current/en-smm-apc-web-guidelines.html).

# Documentation

The complete n_ToF architecture is described in [n_ToF Software Architecture](https://apc-dev.web.cern.ch/docs/drafts/current/ntof-software-architecture.html).

# Build

To build this application application (assuming that Node.js is installed on your machine):
```bash
# Serve pages on port 8080
npm run serve
```

# Deploy

TODO
