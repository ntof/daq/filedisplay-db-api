// @ts-check
const
  Server = require('./Server'),
  _ = require('lodash');

var isStub = false;

/** @type {AppServer.Config} */
var config;
try {
  // @ts-ignore
  config = require('/etc/app/config'); /* eslint-disable-line global-require */
}
catch (e) {
  // @ts-ignore
  config = require('./config-stub'); /* eslint-disable-line global-require */
  isStub = true;
}

config.port = _.defaultTo(config.port, 8080);
config.basePath = _.defaultTo(config.basePath, '');

var server = new Server(config);

// @ts-ignore
if (!module.parent) {
  (async function() {
    /* we're called as a main, let's listen */
    await server.listen(() => {
      console.log(`Server listening on port http://localhost:${config.port}`);
    });
    if (isStub) {
      server.db.createStub();
    }
  }());

}
else {
  /* export our server */
  module.exports = server;
}
