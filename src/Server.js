// @ts-check
const
  express = require('express'),
  helmet = require('helmet'),
  path = require('path'),
  { assign, attempt, noop, cloneDeep, forEach, isNil, get } = require('lodash'),
  cors = require('cors'),
  { makeDeferred } = require('@cern/prom'),

  debug = require('debug')('app:server'),
  logger = require('./httpLogger'),
  Database = require('./db'),
  schema = require('./db/schema');

/**
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 * @typedef {import('express').NextFunction} NextFunction
 */

class Server {
  /**
   * @param {AppServer.Config} config
   */
  constructor(config) {
    this.config = config;
    this.app = express();
    this._prom = this.prepare(config);
  }

  /**
   * @param {AppServer.Config} config
   */
  async prepare(config) {
    if (process.env.NODE_ENV === 'production') {
      this.app.use(helmet());
    }
    logger.register(this.app);

    this.router = express.Router();

    this.app.set('view engine', 'pug');
    this.app.set('views', path.join(__dirname, 'views'));

    this.router.get('/', (req, res) => res.render('index', config));

    this.db = new Database(config.basePath + '/db', config.port, schema, config.db);
    
    this.router.use('/api', cors(), this.db.routerApi());
    this.router.use('/db', cors(), this.db.routerDb());

    this.app.use(config.basePath, this.router);

    // default route
    this.app.use(function(req, res, next) {
      next({ status: 404, message: 'Not Found' });
    });

    // error handler
    this.app.use(function(
      /** @type {any} */ err,
      /** @type {Request} */ req,
      /** @type {Response} */res,
      /** @type {NextFunction} */ next) { /* eslint-disable-line */ /* jshint ignore:line */
      res.locals.message = err.message;
      res.locals.error = req.app.get('env') === 'development' ? err : {};
      res.locals.status = err.status || 500;
      res.status(err.status || 500);
      res.render('error', { baseUrl: config.basePath });
    });
  }

  close() {
    if (this.server) {
      this.server.close();
      this.server = null;
    }
  }

  /**
   * @param {() => any} cb
   */
  async listen(cb) {
    await this._prom;
    const def = makeDeferred();
    /* we're called as a main, let's listen */
    var server = this.app.listen(this.config.port, () => {
      this.server = server;
      def.resolve(undefined);
      return attempt(cb || noop);
    });
    return def.promise;
  }

  address() {
    if (this.server) {
      return this.server.address();
    }
    return null;
  }
}

module.exports = Server;
