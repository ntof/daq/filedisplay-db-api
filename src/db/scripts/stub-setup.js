#!/usr/bin/env node
// @ts-check
/* eslint-disable max-lines */

const
  debug = require('debug')('app:stub'),
  { createGunzip } = require('zlib'),
  readline = require('readline'),
  fs = require('fs'),
  path = require('path'),
  knex = require('knex'),
  { first, omit, map, transform } = require('lodash');

/**
 * @typedef {import('knex').TableBuilder} TableBuilder
 */

/**
 * @param {knex} db
 * @param {string} table
 * @param {(table: TableBuilder) => any} fun
 */
async function createTable(db, table, fun) {
  if (!await db.schema.hasTable(table)) {
    await db.schema.createTable(table, fun);
  }
}

/**
 * @param {knex} db
 */
async function createSchema(db) {
  debug('Building schema');

  await createTable(db, 'MATERIALS', function(table) {
    table.increments('MAT_ID');
    table.string('MAT_TYPE', 20).notNullable();
    table.string('MAT_TITLE', 100).notNullable();
    table.string('MAT_COMPOUND', 25).notNullable();
    table.integer('MAT_MASS_NUMBER').notNullable();
    table.float('MAT_MASS_MG', 126);
    table.integer('MAT_ATOMIC_NUMBER');
    table.float('MAT_THICKNESS_UM', 126);
    table.float('MAT_DIAMETER_MM', 126);
    table.integer('MAT_STATUS').notNullable();
    table.float('MAT_AREA_DENSITY_UM_CM2', 126);
    table.string('MAT_DESCRIPTION', 4000).notNullable();
    table.index([ 'MAT_TYPE', 'MAT_COMPOUND' ], 'MAT_TYPE_COMPOUND_IDX');
  });

  await createTable(db, 'MATERIALS_SETUPS', function(table) {
    table.increments('MAT_SETUP_ID');
    table.string('MAT_SETUP_NAME', 255).unique();
    table.string('MAT_SETUP_DESCRIPTION', 255);
    table.integer('MAT_SETUP_EAR_NUMBER').notNullable();
    table.integer('MAT_SETUP_OPER_YEAR').notNullable();
    table.index([ 'MAT_SETUP_EAR_NUMBER', 'MAT_SETUP_OPER_YEAR' ],
      'MAT_EAR_YEAR_IDX');
  });

  await createTable(db, 'DETECTORS', function(table) {
    table.increments('DET_ID');
    table.string('DET_NAME', 50).notNullable().unique();
    table.string('DET_DESCRIPTION', 255).notNullable();
    table.string('DET_DAQNAME', 255);
    table.string('DET_TRECNUMBER', 255);

    table.index([ 'DET_DAQNAME' ], 'DET_DAQNAME_IDX');
  });

  await createTable(db, 'DETECTORS_SETUPS', function(table) {
    table.increments('DET_SETUP_ID');
    table.string('DET_SETUP_NAME', 255).unique();
    table.string('DET_SETUP_DESCRIPTION', 255);
    table.integer('DET_SETUP_EAR_NUMBER').notNullable();
    table.integer('DET_SETUP_OPER_YEAR').notNullable();
    table.index(
      [ 'DET_SETUP_EAR_NUMBER', 'DET_SETUP_OPER_YEAR', 'DET_SETUP_NAME' ],
      'DET_SETUP_EAR_YEAR_NAME_IDX');
  });

  await createTable(db, 'CONTAINERS', function(table) {
    table.increments('CONT_ID');
    table.string('CONT_NAME', 255).notNullable();
    table.string('CONT_DESCRIPTION', 255);
    table.string('CONT_TRECNUMBER', 255);

    table.index([ 'CONT_NAME' ], 'CONT_NAME_IDX');
  });

  await createTable(db, 'REL_DETECTORS_SETUPS', function(table) {
    table.integer('DET_ID').notNullable()
    .references('DETECTORS.DET_ID').withKeyName('SETUP_DET_ID_FK');
    table.integer('DET_SETUP_ID').notNullable()
    .references('DETECTORS_SETUPS.DET_SETUP_ID').withKeyName('DET_SETUP_ID_FK');
    table.integer('DET_CONT_ID')
    .references('CONTAINERS.CONT_ID').withKeyName('DET_CONT_ID_FK');

    table.primary([ 'DET_SETUP_ID', 'DET_ID' ]);
  });

  await createTable(db, 'REL_MATERIALS_SETUPS', function(table) {
    table.integer('MAT_ID').notNullable()
    .references('MATERIALS.MAT_ID').withKeyName('SETUP_MAT_ID_FK');
    table.integer('MAT_SETUP_ID').notNullable()
    .references('MATERIALS_SETUPS.MAT_SETUP_ID').withKeyName('MAT_SETUP_ID_FK');
    table.integer('MAT_STATUS').notNullable();
    table.integer('MAT_POSITION');

    table.primary([ 'MAT_SETUP_ID', 'MAT_ID' ]);
  });


  await createTable(db, 'RUNS', function(table) {
    table.integer('RUN_NUMBER').primary();
    table.integer('RUN_START').notNullable();
    table.integer('RUN_STOP');
    table.string('RUN_TITLE', 255).notNullable();
    table.string('RUN_DESCRIPTION', 4000);
    table.string('RUN_TITLE_ORIGINAL', 255);
    table.string('RUN_DESCRIPTION_ORIGINAL', 4000);
    table.string('RUN_CASTOR_FOLDER', 255).notNullable();
    table.integer('RUN_TOTAL_PROTONS').defaultTo(0);
    table.string('RUN_RUN_STATUS', 12).notNullable();

    table.string('RUN_DATA_STATUS', 12).notNullable();

    table.integer('RUN_DET_SETUP_ID');

    table.integer('RUN_MAT_SETUP_ID');

    table.index([ 'RUN_START' ], 'RUN_START_IDX');
    table.index([ 'RUN_TITLE' ], 'RUN_TITLE_IDX');
  });

  await createTable(db, 'HV_PARAMETERS', function(table) {
    table.increments('HV_ID');
    table.integer('HV_SLOT_ID').notNullable();
    table.integer('HV_SERIAL_NUMBER').comment('null on old entries');
    table.integer('HV_CHANNEL_ID').notNullable();
    table.string('HV_NAME', 255).notNullable();
    table.float('HV_VO_SET_VOLT', 126).notNullable();
    table.float('HV_IO_SET_AMP', 126).notNullable();
    table.float('HV_TRIP', 126).notNullable();
    table.float('HV_RAMP_UP', 126).notNullable();
    table.float('HV_RAMP_DOWN', 126).notNullable();
    table.integer('HV_V_MAX').notNullable();
    table.integer('HV_RUN_NUMBER').notNullable();
    table.unique([ 'HV_RUN_NUMBER', 'HV_CHANNEL_ID', 'HV_SLOT_ID' ],
      'HV_RUN_CHAN_SLOT_UNIQ');

    table.index([ 'HV_RUN_NUMBER' ], 'HV_RUN_NUMBER_IDX');
  });

  await createTable(db, 'DAQ_INSTRUMENTS', function(table) {
    table.increments('DAQI_ID');
    table.integer('DAQI_CHANNEL').notNullable();
    table.integer('DAQI_MODULE').notNullable();
    table.integer('DAQI_CRATE').notNullable();
    table.integer('DAQI_STREAM').notNullable();
    table.unique([ 'DAQI_STREAM', 'DAQI_CRATE', 'DAQI_MODULE', 'DAQI_CHANNEL' ],
      'DAQ_INSTRUMENTS_UNIQ');
  });

  await createTable(db, 'DAQ_PARAMETERS', function(table) {
    table.increments('PARAM_ID');
    table.integer('PARAM_INDEX').notNullable();
    table.string('PARAM_NAME', 50).notNullable();
    table.string('PARAM_VALUE', 50);
    table.unique([ 'PARAM_INDEX', 'PARAM_NAME', 'PARAM_VALUE' ],
      'DAQ_PARAMETERS_UNIQ');
  });

  await createTable(db, 'TRIGGERS', function(table) {
    table.increments('TRIG_ID');
    table.string('TRIG_SOURCE', 55).notNull();
    table.integer('TRIG_CYCLESTAMP');
    table.integer('TRIG_TRIGGER_NUMBER').notNull();
    table.float('TRIG_INTENSITY', 126);
    table.integer('TRIG_RUN_NUMBER').notNull()
    .references('RUNS.RUN_NUMBER').withKeyName('TRIGGERS_RUN_NUMBER_FK');

    table.index([ 'TRIG_RUN_NUMBER', 'TRIG_TRIGGER_NUMBER'  ],
      'TRIGGERS_RUN_NUMBER_IDX');
  });

  await createTable(db, 'CONFIGURATIONS', function(table) {
    table.increments('CONF_ID');
    table.integer('CONF_STATUS').notNullable();
    table.integer('CONF_RUN_NUMBER').notNullable();
    table.integer('CONF_DAQI_ID').notNullable();
    table.unique([ 'CONF_RUN_NUMBER', 'CONF_DAQI_ID' ],
      'CONFIGURATIONS_RUN_DAQI_UNIQ');

    table.index([ 'CONF_RUN_NUMBER' ], 'CONF_RUN_NUMBER_IDX');
  });

  await createTable(db, 'REL_CONF_PARAMS', function(table) {
    table.integer('PARAM_ID').notNullable()
    .references('DAQ_PARAMETERS.PARAM_ID').withKeyName('CON_PARAM_ID_FK');
    table.integer('CONF_ID').notNullable()
    .references('CONFIGURATIONS.CONF_ID').withKeyName('PARAM_CONF_ID_FK');

    table.primary([ 'CONF_ID', 'PARAM_ID' ]);
  });

  await createTable(db, 'CASTOR_FILES', function(table) {
    table.increments('CF_ID');
    table.string('CF_FILE_NAME', 32).notNull().unique('CASTOR_FILES_NAME_UNIQ');
    table.string('CF_FILE_PATH', 256);
    table.integer('CF_FILE_SIZE').notNull();
    table.integer('CF_CREATED').notNull();
    table.integer('CF_STATUS').notNull();
    table.integer('CF_STREAM').notNull();
    table.integer('CF_EXTENSION').notNull();
    table.integer('CF_RUN_NUMBER').notNull()
    .references('RUNS.RUN_NUMBER').withKeyName('CASTOR_FILES_RUN_NUMBER_FK');

    table.index([ 'CF_RUN_NUMBER' ], 'CF_RUN_NUMBER_IDX');
  });

  await createTable(db, 'CASTOR_FILE_EVENTS', function(table) {
    table.increments('CFE_ID');
    table.integer('CFE_EVENT').notNull();
    table.integer('CFE_OFFSET').notNull();
    table.integer('CFE_SEQUENCE').notNull();
    table.integer('CFE_CF_ID').notNull()
    .references('CASTOR_FILES.CF_ID')
    .withKeyName('CASTOR_FILE_EVENTS_CF_ID_FK');

    table.unique([ 'CFE_CF_ID', 'CFE_EVENT' ], 'CFE_CF_EVENT_UNIQ');
  });
}

/**
 * @param {knex} db
 * @param {string} file
 */
async function insertData(db, file) {
  const gz = createGunzip();
  fs.createReadStream(file).pipe(gz);

  var trx = await db.transaction();
  const reader = readline.createInterface({
    input: gz, terminal: false
  });
  var i = 0;
  var stmt = '';

  for await (const line of reader) {
    if (line.length <= 0) {
      continue;
    }
    else if (line[line.length - 1] === ';') {
      stmt = stmt + line.slice(0, -1);
    }
    else {
      stmt += line.replace(/; $/g, ';') + '\n';
      continue;
    }
    await trx.raw(stmt);
    stmt = '';

    if ((++i % 1000) === 0) {
      debug('rows inserted:', i);
      await trx.commit();
      trx = await db.transaction();
    }
  }
  debug('rows inserted:', i);
  await trx.commit();
}

/**
 * @param {knex} db
 */
async function updateData(db) {
  /* update stubs to always work properly :) */
  const today = new Date();
  const thisYear = today.getFullYear();
  const thisMonth = today.getMonth();

  await db('MATERIALS_SETUPS')
  .update({ MAT_SETUP_OPER_YEAR: thisYear })
  .where('MAT_SETUP_OPER_YEAR', 2018);

  await db('DETECTORS_SETUPS')
  .update({ DET_SETUP_OPER_YEAR: thisYear })
  .where('DET_SETUP_OPER_YEAR', 2018);

  // Add some containers + detectors
  await db.raw(db('CONTAINERS').insert({
    CONT_ID: 1, CONT_NAME: 'test container',
    CONT_DESCRIPTION: 'Sample container with some description',
    CONT_TRECNUMBER: 1234
  }).toString().replace(/^insert/, 'insert or replace'));

  await db.raw(db('DETECTORS').insert({
    DET_ID: 1, DET_NAME: 'test detector',
    DET_DESCRIPTION: 'Sample detector with some description',
    DET_DAQNAME: 'DAQNAME', DET_TRECNUMBER: 12345
  }).toString().replace(/^insert/, 'insert or replace'));

  await db.raw(db('DETECTORS').insert({
    DET_ID: 2, DET_NAME: 'test detector 2',
    DET_DESCRIPTION: 'Sample detector with some description 2',
    DET_DAQNAME: 'DAQNAME 2', DET_TRECNUMBER: 123456
  }).toString().replace(/^insert/, 'insert or replace'));

  await db.raw(db('REL_DETECTORS_SETUPS').insert({
    DET_ID: 1, DET_CONT_ID: 1, DET_SETUP_ID: 22
  }).toString().replace(/^insert/, 'insert or replace'));

  debug('stub data udpated');
}

/**
 * @param {knex} db
 */
async function stubSetup(db) {
  await createSchema(db);
  await insertData(db, path.join(__dirname, 'sample.sql.dump.gz'));
  await updateData(db);
}

// @ts-ignore
if (!module.parent) {
  const config = require('../../config-stub'); // eslint-disable-line global-require
  const db = knex(omit(config.db, 'definition'));

  stubSetup(db)
  .catch((err) => console.log('Error:', err))
  .finally(() => db.destroy());
}
else {
  /* export our server */
  module.exports = stubSetup;
}
