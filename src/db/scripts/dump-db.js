// @ts-check
const
  _ = require('lodash'),
  // @ts-ignore
  config = require('../../../config'),
  knex = require('knex');

/* do not use node-oracledb LOB objects, dump content as a Buffer */
const oracledb = require('oracledb');
oracledb.fetchAsBuffer = [ oracledb.BLOB ];

/**
 * @typedef {{
 *  db: knex
 *  out: knex,
 *  history: { [tableName: string]: Set<string> }
 * }} Env
 */

/**
 *
 * @param {knex} k
 * @param {string} tableName
 * @param {any} row
 */
function sqlInsert(k, tableName, row) {
  row = _.omitBy(row, (value, key) => (key[0] === '_'));
  row = _.mapValues(row,
    (value) => (_.isString(value) ? value.replace(/;\r?\n/g, '; \n') : value));
  return k(tableName).insert(row)
  .toString().replace(/^insert/, 'insert or replace');
}

/**
 *
 * @param {Env} env
 * @param {string} tableName
 * @param {any[]} rows
 * @param {string} rowIdName
 */
function dumpRow(env, tableName, rows, rowIdName) {
  if (!_.get(env, [ 'history', tableName ])) {
    _.set(env, [ 'history', tableName ], new Set());
  }
  const hist = env.history[tableName];

  if (_.isArray(rows)) {
    rows = rows.filter((row) => !hist.has(row[rowIdName]));
    _.forEach(rows, (row) => {
      hist.add(row[rowIdName]);
      console.log(sqlInsert(env.out, tableName, row) + ';');
    });
  }
  else if (_.isObject(rows) && !hist.has(rows[rowIdName])) {
    hist.add(rows[rowIdName]);
    console.log(sqlInsert(env.out, tableName, rows) + ';');
  }
}

/**
 * @param {Env} env
 * @param {number[]} ids
 */
async function dumpMaterialsSetups(env, ids) {
  await env.db('REL_MATERIALS_SETUPS').select('*')
  .whereIn('MAT_SETUP_ID', ids)
  .then(async (rows) => {
    for (const row of rows) {
      row._UUID = row.MAT_SETUP_ID + ':' + row.MAT_ID;

      const materials = await env.db('MATERIALS').select('*')
      .where('MAT_ID', row.MAT_ID);
      dumpRow(env, 'MATERIALS', materials, 'MAT_ID');
    }
    dumpRow(env, 'REL_MATERIALS_SETUPS', rows, '_UUID');
  });
}

/**
 * @param {Env} env
 * @param {number[]} ids
 */
async function dumpDetectorsSetups(env, ids) {
  await env.db('REL_DETECTORS_SETUPS').select('*')
  .whereIn('DET_SETUP_ID', ids)
  .then(async (rows) => {
    for (const row of rows) {
      row._UUID = row.DET_SETUP_ID + ':' + row.DET_ID;

      await env.db('DETECTORS').select('*').where('DET_ID', row.DET_ID)
      .then((rows) => dumpRow(env, 'DETECTORS', rows, 'DET_ID'));

      await env.db('CONTAINERS').select('*').where('DET_CONT_ID', row.DET_ID)
      .then((rows) => dumpRow(env, 'CONTAINERS', rows, 'CONT_ID'));
    }
    dumpRow(env, 'REL_DETECTORS_SETUPS', rows, '_UUID');
  });
}

/**
 * @param {Env} env
 * @param {number[]} ids
 */
async function dumpRuns(env, ids) {
  await env.db('CONFIGURATIONS').select('*')
  .whereIn('CONF_RUN_NUMBER', ids)
  .then(async (rows) => {
    for (const chunk of _.chunk(rows, 250)) {
      const rel = await env.db('REL_CONF_PARAMS').select('*')
      .whereIn('CONF_ID', _.map(chunk, 'CONF_ID'));
      rel.forEach((r) => (r._UUID = `${r.PARAM_ID}:${r.CONF_ID}`));

      await env.db('DAQ_PARAMETERS').select('*')
      .whereIn('PARAM_ID', _.uniq(_.map(rel, 'PARAM_ID')))
      .then((rows) => dumpRow(env, 'DAQ_PARAMETERS', rows, 'PARAM_ID'));

      dumpRow(env, 'CONFIGURATIONS', rows, 'CONF_ID');
      dumpRow(env, 'REL_CONF_PARAMS', rel, '_UUID');
    }
  });

  await env.db('TRIGGERS').select('*')
  .whereIn('TRIG_RUN_NUMBER', ids.slice(0, 3)) // just for 3 runs
  .then(async (rows) => {
    dumpRow(env, 'TRIGGERS', rows, 'TRIG_ID');
  });

  await env.db('HV_PARAMETERS').select('*')
  .whereIn('HV_RUN_NUMBER', ids)
  .then(async (rows) => {
    dumpRow(env, 'HV_PARAMETERS', rows, 'HV_ID');
  });

  await env.db('CASTOR_FILES').select('*')
  .whereIn('CF_RUN_NUMBER', ids)
  .then(async (rows) => {

    await env.db('CASTOR_FILE_EVENTS').select('*')
    .whereIn('CFE_CF_ID', _.uniq(_.map(rows, 'CF_ID')).slice(0, 100))
    .then((rows) => dumpRow(env, 'CASTOR_FILE_EVENTS', rows, 'CFE_ID'));
    dumpRow(env, 'CASTOR_FILES', rows, 'HV_ID');
  });
}

/** @type {Env} */
const env = {
  db: knex(config.db),
  out: knex({ client: 'sqlite3', useNullAsDefault: true }),
  history: {}
};

(async () => {
  await env.db('MATERIALS_SETUPS').select('*')
  .where('MAT_SETUP_OPER_YEAR', 2017).limit(50)
  .then(async (rows) => {
    dumpRow(env, 'MATERIALS_SETUPS', rows, 'MAT_SETUP_ID');
    await dumpMaterialsSetups(env, _.map(rows, 'MAT_SETUP_ID'));
  });

  await env.db('MATERIALS_SETUPS').select('*')
  .where('MAT_SETUP_OPER_YEAR', 2018).limit(50)
  .then(async (rows) => {
    dumpRow(env, 'MATERIALS_SETUPS', rows, 'MAT_SETUP_ID');
    await dumpMaterialsSetups(env, _.map(rows, 'MAT_SETUP_ID'));
  });

  await env.db('DETECTORS_SETUPS').select('*')
  .where('DET_SETUP_OPER_YEAR', 2017).limit(50)
  .then(async (rows) => {
    dumpRow(env, 'DETECTORS_SETUPS', rows, 'DET_SETUP_ID');
    await dumpDetectorsSetups(env, _.map(rows, 'DET_SETUP_ID'));
  });

  await env.db('DETECTORS_SETUPS').select('*')
  .where('DET_SETUP_OPER_YEAR', 2018).limit(50)
  .then(async (rows) => {
    dumpRow(env, 'DETECTORS_SETUPS', rows, 'DET_SETUP_ID');
    await dumpDetectorsSetups(env, _.map(rows, 'DET_SETUP_ID'));
  });

  await env.db('DAQ_INSTRUMENTS').select('*')
  .then((rows) => dumpRow(env, 'DAQ_INSTRUMENTS', rows, 'DAQI_ID'));

  await env.db('RUNS').select('*')
  .where('RUN_START', '>=', Date.parse('01 Jan 2017'))
  .where('RUN_START', '<=', Date.parse('01 Jan 2018'))
  .limit(500).orderBy('RUN_NUMBER', 'desc')
  .then(async (runs) => {
    dumpRow(env, 'RUNS', runs, 'RUN_NUMBER');
  });

  await env.db('RUNS').select('*')
  .where('RUN_START', '>=', Date.parse('01 Jan 2018'))
  .where('RUN_START', '<=', Date.parse('01 Jan 2019'))
  .limit(2000).orderBy('RUN_NUMBER', 'desc')
  .then(async (runs) => {
    _.forEach(runs, (r) => {
      if (!env.history['MATERIALS_SETUPS'].has(r.RUN_MAT_SETUP_ID)) {
        r.RUN_MAT_SETUP_ID = null;
      }
      if (!env.history['DETECTORS_SETUPS'].has(r.RUN_DET_SETUP_ID)) {
        r.RUN_DET_SETUP_ID = null;
      }
    });
    dumpRow(env, 'RUNS', runs, 'RUN_NUMBER');
    await dumpRuns(env, _.map(runs, 'RUN_NUMBER').slice(0, 5));
  });
})()
.finally(() => {
  env.db.destroy();
});
