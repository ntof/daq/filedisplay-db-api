// @ts-check
const
  { assign, cloneDeep, get, isNil, noop,
    toNumber, set, map, omit, findIndex,
    filter, isEmpty } = require('lodash'),
  stubSetup = require('./scripts/stub-setup'),
  debug = require('debug')('app:db'),
  express = require('express'),
  sa = require('superagent'),
  KnexServer = require('@cern/mrest');

/**
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 */

const doc = require('./doc');

const Areas = {
  EAR1: { prefix: 1, minValue: 100000, maxValue: 199999 },
  EAR2: { prefix: 2, minValue: 200000, maxValue: 299999 }
};

class Database extends KnexServer {
  /**
   *
   * @param {string} basePath
   * @param {string} port
   * @param {mrest.DatabaseDescription} schema
   * @param {mrest.KnexServer.Options} config
   */
  constructor(basePath, port, schema, config) {
    super(config, schema, assign(cloneDeep(doc), { basePath }));
    this.basePath = basePath;
    this.port = port;
    this.schema = schema;
  }

  baseUrl() {
    return `localhost:${this.port}${this.basePath}`;
  }

  routerApi() {
    const router = express.Router();

    // Custom endpoints
    router.get('/ping', (req, res) => res.send('pong'));
    router.get('/runs',
      KnexServer.ExpressHelpers.wrap(this.retrieveRuns.bind(this)));
    router.get('/events',
      KnexServer.ExpressHelpers.wrap(this.retrieveEvents.bind(this)));
    router.get('/castorPath',
      KnexServer.ExpressHelpers.wrap(this.retrieveCastorPath.bind(this)));
    router.get('/experiment',
      KnexServer.ExpressHelpers.wrap(this.retrieveExperiment.bind(this)));
    return router;
  }

  routerDb() {
    const router = express.Router();
    super.register(router);
    this.routerObj = router;
    return router;
  }

  async createStub() {
    return stubSetup(this.knex);
  }

  /**
   * @param {Request} req
   * @param {string} name
   * @param {boolean} isMandatory
   * @returns {any}
   */
  _getParamFromQuery(req, name, isMandatory = true) {
    const param = /** @type {any} */(get(req.query, name));
    if (isMandatory && isNil(param))
      throw `${name} not provided`;
    return param;
  }

  /**
   * @param {Request} req
   * @returns {number}
   */
  _getRunNumberFromQuery(req) {
    let runNumber = this._getParamFromQuery(req, 'run');
    runNumber = toNumber(runNumber);
    if (isNaN(runNumber) ||
      runNumber < Areas.EAR1.minValue ||
      runNumber > Areas.EAR2.maxValue)
      throw "run not valid";
    return runNumber;
  }

  /**
   * @param {Request} req
   * @returns {number}
   */
  _getEventNumberFromQuery(req) {
    let eventNumber = this._getParamFromQuery(req, 'event');
    eventNumber = toNumber(eventNumber);
    if (isNaN(eventNumber))
      throw "event not valid";
    return eventNumber;
  }

  /**
   * @param {number} runNumber
   */
  async _getRun(runNumber) {
    return this.tables['runs'].get( // @ts-ignore fake Request
      { get: noop, params: { id: runNumber } }, { set: noop });
  }

  /**
   * @param {Request} req
   * @param {Response} res
   */
  async retrieveRuns(req, res) {
    const area = this._getParamFromQuery(req, 'area');
    const areaInfo = get(Areas, area);
    if (isNil(areaInfo))
      throw "area not supported";

    const query = {
      max: 100000,
      filter: {
        $and: [
          { runNumber: { $gte: areaInfo.minValue } },
          { runNumber: { $lte: areaInfo.maxValue } }
        ]
      }
    };
    const runs = await this.tables['runs'].search( // @ts-ignore fake Request
      { get: noop, query }, { set: noop });

    return map(runs, 'runNumber'); // Get the list of runNumbers only

    // Maybe better via kenex directly?
    /*
    const ret = await this.knex('RUNS').select('RUN_NUMBER')
    .where('RUN_NUMBER', '>', areaInfo.minValue)
    .andWhere('RUN_NUMBER', '<', areaInfo.maxValue);
    return map(ret, 'RUN_NUMBER'); // Get the list of runNumbers only
    */
  }

  /**
   * @param {Request} req
   * @param {Response} res
   */
  async retrieveEvents(req, res) {
    const runNumber = this._getRunNumberFromQuery(req);
    console.info('retrieveEvents for', runNumber);

    const query = {
      max: 100000,
      filter: { runNumber }
    };
    const castorFiles = await this.tables['castorFiles'].search( // @ts-ignore fake Request
      { get: noop, query }, { set: noop });

    // Using for loop so we are sure we do 1 db search call per time
    for (var i = 0; i < castorFiles.length; i++) {
      const cf = castorFiles[i];
      const query = {
        max: 100000,
        filter: { castorFileID: get(cf, 'id') }
      };
      const events = await this.tables['castorFileEvents'].search( // @ts-ignore fake Request
        { get: noop, query }, { set: noop });
      set(cf, 'events', map(events, (e) => omit(e, 'castorFileID')));
    }
    return castorFiles;
  }

  /**
   * @param {Request} req
   * @param {Response} res
   */
  async retrieveCastorPath(req, res) {
    const eventNumber = this._getEventNumberFromQuery(req);
    const castorFiles = await this.retrieveEvents(req, res);

    const cfEvent = filter(castorFiles, (cf) => {
      const eventIndex = findIndex(cf.events, (e) => e.event === eventNumber);
      return eventIndex !== -1;
    });

    if (isEmpty(cfEvent))
      throw "event not found";
    else if (cfEvent.length > 1) // should never happen
      throw "same event appears in more file";
    else {
      const path = get(cfEvent, [ 0, 'path' ]);
      const filename = get(cfEvent, [ 0, 'filename' ]);
      return {
        path: `${path}/${filename}`
      };
    }
  }

  /**
   * @param {Request} req
   * @param {Response} res
   */
  async retrieveExperiment(req, res) {
    const runNumber = this._getRunNumberFromQuery(req);
    const run = await this._getRun(runNumber);
    if (isNil(run))
      throw "run not found";
    return {
      experiment: get(run, 'castorFolder', '')
    };
  }
}

module.exports = Database;
