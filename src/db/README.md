
# Dev Database

A development database is automatically configured if no configuration is
provided to the project (in `/etc/app/config`).

This development database is an SQLite database named `db.sqlite` and located
at the root of this project.

## Generating stub data

The `dump-db.js` script can be used to generate `sample.sql.dump.gz` file.

It requires a `config.js` file at the root of the project with the following
content:
```js
module.exports = {
  db: {
    client: "oracledb",
    connection: {
      user: "ntofdev",
      password: "xxxx",
      connectString: "devdb11-s.cern.ch:10121/devdb11_s.cern.ch"
    }
  }
}
```

Once this file is created you can simply run:
```bash
node ./src/db/scripts/dump-db.js | gzip -9 > ./src/db/scripts/sample.sql.dump.gz
```
